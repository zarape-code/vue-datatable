# Vue Datatable

Datatable con Vue, más info en [blog.zarape.io](https://blog.zarape.io)

## Iniciar el proyecto

```bash
# Instala las dependencias
$ npm install

# Arrancar en modo desarrollo con hot reload en localhost:8080
$ npm start

# Compilar y ejecuta el proyecto para producción
$ npm run build
$ npm run start
```

## Estructura del proyecto
```sh
\                   # Archivos de configuración
  |-src             # Carpeta para el contenido del proyecto
    |-components    # Carpeta para los componentes del proyecto
  |-App.vue         # Componente principal del proyecto
  |-main.js         # Archivo principal de configuración de vue
```

## Tecnología empleada
* [Vuejs](https://vuejs.org) - Framework JS.

___
### Autor
Creado por [Erick Uribe](https://www.linkedin.com/in/erickuribedelapaz/) para zarape.io 🇲🇽
